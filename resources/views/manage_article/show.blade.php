	@extends('Welcome')

	@section('css')
		<link rel="stylesheet" type="text/css" href="{{ asset("asset/css/bootstrap.min.css") }}">
		<link rel="stylesheet" type="text/css" href="{{ asset("asset/css/sticky-footer-navbar.css") }} ">
	@stop

	@section('title')
		Show Page
	@stop

	@section('content')
		<div class="page-header">
        	<h1>Show Article</h1>
        	@foreach($details as $detail)
        	<h4 class="article-header">{{ $detail->title }}</h4>
	    	<p class="article-desc">{{ $detail->desc }}</p>
        	@endforeach
      	</div>
	    
    @stop

    @section('footer')

     	<div class="container">
        	<p class="text-muted">&copy; Copy right in 2015 by AirXpress. All right Reserve. Designed by Mr.CHHEAN Samphors.</p>
      	</div>

      	<script type="text/javascript" language="javascript" src="{{ asset("asset/js/jquery.min.js") }}"></script>
      	<script type="text/javascript" language="javascript" src="{{ asset("asset/js/bootstrap.min.js") }}"></script>
      	<script type="text/javascript" language="javascript" src="{{ asset("asset/js/ie-emulation-modes-warning.js") }}"></script>
      	<script type="text/javascript" language="javascript" src="{{ asset("asset/js/ie10-viewport-bug-workaround.js") }}"></script>
    @stop

