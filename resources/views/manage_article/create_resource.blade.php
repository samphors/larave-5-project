	@extends('Welcome')

	@section('css')
		<link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="asset/css/sticky-footer-navbar.css">
	@stop

	@section('title')
		Create Resource
	@stop

	@section('content')
		<div class="page-header">
        	<h1>Create New Resource</h1>
      	</div>

      	<div class="row">
      		<div class="col-md-12">
      			<form class="form-horizental">
      				<input type="text" class="form-control" name="title" placeholder="Title" />
      				<br />
      				<textarea name="desc" class="form-control"></textarea>
      				<br />
      				<input type="submit" value="Submit" class="btn btn-primary" />
      			</form>
      		</div>
      	</div>
    @stop

    @section('footer')
     	<div class="container">
        	<p class="text-muted">&copy; Copy right in 2015 by AirXpress. All right Reserve. Designed by Mr.CHHEAN Samphors.</p>
      	</div>

      	<script type="text/javascript" language="javascript" src="asset/js/jquery.min.js"></script>
      	<script type="text/javascript" language="javascript" src="asset/js/bootstrap.min.js"></script>
      	<script type="text/javascript" language="javascript" src="asset/js/ie-emulation-modes-warning.js"></script>
      	<script type="text/javascript" language="javascript" src="asset/js/ie10-viewport-bug-workaround.js"></script>
    @stop

