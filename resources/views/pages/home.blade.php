	@extends('Welcome')

	@section('css')
		<link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="asset/css/sticky-footer-navbar.css">
	@stop

	@section('title')
		Home Page
	@stop

	@section('content')
		<div class="page-header">
        	<h1>List All Articles</h1>
      	</div>
	    @foreach($articles as $article)
	    	<h4 class="article-header">{{ $article->title }}</h4>
	    	<p class="article-desc">{{ $article->desc }}</p>
	    	<a href="detail/{{ $article->id }}">Read more</a>
	    	<hr />
	    @endforeach
    @stop

    @section('footer')
     	<div class="container">
        	<p class="text-muted">&copy; Copy right in 2015 by AirXpress. All right Reserve. Designed by Mr.CHHEAN Samphors.</p>
      	</div>

      	<script type="text/javascript" language="javascript" src="asset/js/jquery.min.js"></script>
      	<script type="text/javascript" language="javascript" src="asset/js/bootstrap.min.js"></script>
      	<script type="text/javascript" language="javascript" src="asset/js/ie-emulation-modes-warning.js"></script>
      	<script type="text/javascript" language="javascript" src="asset/js/ie10-viewport-bug-workaround.js"></script>
    @stop

