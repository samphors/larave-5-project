	@extends('Welcome')

	@section('css')
		<link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="asset/css/sticky-footer-navbar.css">
	@stop

	@section('title')
		About Page
	@stop

	@section('content')
		<div class="page-header">
        	<h1>Bio Page</h1>
      	</div>
	    <p class="lead">Pin a fixed-height footer to the bottom of the viewport in desktop browsers with this custom HTML and CSS. A fixed navbar has been added with <code>padding-top: 60px;</code> on the <code>body > .container</code>.</p>
	    <p>Back to <a href="../sticky-footer">the default sticky footer</a> minus the navbar.</p>
    @stop

    @section('footer')
     	<div class="container">
        	<p class="text-muted">&copy; Copy right in 2015 by AirXpress. All right Reserve. Designed by Mr.CHHEAN Samphors.</p>
      	</div>

      	<script type="text/javascript" language="javascript" src="asset/js/jquery.min.js"></script>
      	<script type="text/javascript" language="javascript" src="asset/js/bootstrap.min.js"></script>
      	<script type="text/javascript" language="javascript" src="asset/js/ie-emulation-modes-warning.js"></script>
      	<script type="text/javascript" language="javascript" src="asset/js/ie10-viewport-bug-workaround.js"></script>
    @stop

