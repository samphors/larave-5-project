<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

 Route::get('/', 'Manage_Article_Controller@index');
 Route::get('/detail/{id}', 'Manage_Article_Controller@show');

 Route::get('/about', function() {
 	return view('pages.about');
 });

// Route::get('/contact', function() {
// 	return view('pages.contact');
// });
Route::get('/create-resource','Manage_Article_Controller@create');

